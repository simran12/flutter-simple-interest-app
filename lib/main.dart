import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Simple Interest App",
    home: SIForm(),
    theme: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.indigoAccent,
      accentColor: Colors.indigoAccent,
    ),
  ));
}

class SIForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SIForm();
  }
}

class _SIForm extends State<SIForm> {

  var _formKey = GlobalKey<FormState>();

  var currencies = ["Rupee", "Dollar", "Pound"];

  var _currentItem = "";

  var amt = "";

  @override
  void initState(){
    super.initState();
    _currentItem = currencies[0];
  }

  TextEditingController pController = TextEditingController();
  TextEditingController rController = TextEditingController();
  TextEditingController tController = TextEditingController();



  @override
  Widget build(BuildContext context) {


    TextStyle textStyle = Theme.of(context).textTheme.title;

    return Scaffold(
        appBar: AppBar(
          title: Text("Simple Interest Calculator"),
        ),
        body: Padding(padding: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
          child: ListView(
            children: <Widget>[
              picture(),
              Padding(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: TextFormField(
                  controller: pController,
                  keyboardType: TextInputType.number,
                  style: textStyle,
                  validator: (String value){
                    if(value.isEmpty){
                      return 'Please enter the amount';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: "Principal",
                      hintText: "Enter Principal e.g 12000",
                      labelStyle: textStyle,
                      errorStyle: TextStyle(
                        color: Colors.limeAccent,
                        fontSize: 15.0,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: TextFormField(
                  controller: rController,
                  keyboardType: TextInputType.number,
                  style: textStyle,
                  validator: ( value){
                    if(value.isEmpty){
                      return "Please enter the rate of interest.";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: "Rate of Interest",
                      hintText: "In Percent",
                      labelStyle: textStyle,
                    errorStyle: TextStyle(
                        color: Colors.limeAccent,
                        fontSize: 15.0,),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: TextFormField(
                      controller: tController,
                      keyboardType: TextInputType.number,
                      style: textStyle,
                      validator: ( value){
                        if(value.isEmpty){
                          return "Please enter the time.";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: "Term",
                          hintText: "Time in years",
                          labelStyle: textStyle,
                        errorStyle: TextStyle(
                            color: Colors.limeAccent,
                            fontSize: 15.0,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0))),
                    ),
                  )),
                  Container(
                    width: 25.0,
                  ),
                  Expanded(
                      child: DropdownButton(
                          items: currencies.map((String value) {
                            return DropdownMenuItem(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: _currentItem,
                          onChanged: (String newValue) {
                            _onDropDown(newValue);
                          }))
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          color: Theme.of(context).accentColor,
                          textColor: Colors.black,
                          child: Text(
                            "Calculate",
                            textScaleFactor: 1.5,
                          ),
                          onPressed: () {
                            setState(() {
                              if(_formKey.currentState.validate()){
                                this.amt = calcTotal();
                            }
                            } );
                          },
                        ),
                      ),
                      Expanded(
                        child: RaisedButton(
                          color: Theme.of(context).primaryColorDark,
                          textColor: Colors.white,
                          child: Text( "Reset",textScaleFactor: 1.5,),
                          onPressed: () {
                            reset();
                          },
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(this.amt, style: textStyle,)
              )
            ],
          )),
        ));
  }

  Widget picture() {
    AssetImage assetImage = AssetImage("images/money.png");
    Image image = Image(
      image: assetImage,
      width: 125.0,
      height: 125.0,
    );
    return Container(
      child: image,
      margin: EdgeInsets.all(50.0),
    );
  }

  void _onDropDown(String newValue) {
    setState(() {
      this._currentItem = newValue;
    });
  }

  String calcTotal() {
    double p = double.parse(pController.text);
    double r = double.parse(rController.text);
    double t = double.parse(tController.text);

    double amount = p + (p * r * t) / 100;

    String result = "After $t years your investment will be worth $amount.";

    return result;
  }

  void reset(){
    pController.text="";
    rController.text="";
    tController.text="";
    _currentItem = currencies[0];
    amt="";

  }
}
